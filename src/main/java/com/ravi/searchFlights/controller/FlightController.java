package com.ravi.searchFlights.controller;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.ravi.searchFlights.model.FlightDTO;
import com.ravi.searchFlights.util.FlightUtil;

/**
 * Flight controller class
 * 
 * @author Ravi
 *
 */
@Controller
public class FlightController {
	public static Log log = LogFactory.getLog(FlightController.class);
	private static List<FlightDTO> flightList = new ArrayList<FlightDTO>();

	/**
	 * Get flights with origin and destination.
	 * 
	 * @param origin
	 * @param destination
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{origin}/{destination}", method = RequestMethod.GET)
	public String getFlights(@PathVariable("origin") String origin, @PathVariable("destination") String destination,
			Model model) {
		log.info("Requested For, origin = " + origin + ", destination=" + destination);
		List<FlightDTO> list = readFromCSV("provider1.csv", ',');
		if (list != null) {
			flightList.addAll(list);
			list.clear();
		}
		list = readFromCSV("provider2.csv", ',');
		if (list != null) {
			flightList.addAll(list);
			list.clear();
		}
		list = readFromCSV("provider3.csv", '|');
		if (list != null) {
			flightList.addAll(list);
			list.clear();
		}
		log.info("Total Available Flights::" + flightList.size());
		Map<Integer, FlightDTO> map = new HashMap<Integer, FlightDTO>();
		for (FlightDTO flight : flightList) {
			if (flight.getOrigin().equals(origin) && flight.getDestination().equals(destination)) {
				// make date format identical for departure and destination time
				flight.setDestination_time(FlightUtil.getCommonDate(flight.getDestination_time()));
				flight.setDeparture_time(FlightUtil.getCommonDate(flight.getDeparture_time()));
				// Remove duplicate entry by adding hash value in map
				map.put(flight.hashCode(), flight);
			}
		}
		if (list != null)
			list.clear();

		list.addAll(map.values());
		// Sort flight list 1st with price and 2nd with departure time
		Collections.sort(list);
		log.info("Sorted Flights::" + list.size());
		model.addAttribute("flightList", list);
		model.addAttribute("origin", origin);
		model.addAttribute("destination", destination);
		return "view";
	}

	/**
	 * Method to read data from csv file
	 * 
	 * @param source
	 * @param delimiter
	 * @return
	 */
	private List<FlightDTO> readFromCSV(String source, char delimiter) {
		List<FlightDTO> list = null;
		try {
			/**
			 * Reading the CSV File Delimiter is comma Default Quote character is double
			 * quote Start reading from line 1
			 */
			ClassLoader classLoader = getClass().getClassLoader();
			CSVReader csvReader = new CSVReader(new FileReader(classLoader.getResource(source).getFile()), delimiter,
					CSVParser.DEFAULT_QUOTE_CHARACTER, 1);
			// mapping of columns with their positions
			ColumnPositionMappingStrategy mappingStrategy = new ColumnPositionMappingStrategy();
			// Set mappingStrategy type to FlightDTO Type
			mappingStrategy.setType(FlightDTO.class);
			// Fields in FlightDTO Bean
			String[] columns = new String[] { "origin", "departure_time", "destination", "destination_time", "price" };
			// Setting the colums for mappingStrategy
			mappingStrategy.setColumnMapping(columns);
			// create instance for CsvToBean class
			CsvToBean ctb = new CsvToBean();
			// parsing csvReader(Employee.csv) with mappingStrategy
			list = ctb.parse(mappingStrategy, csvReader);
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		return list;
	}
}
