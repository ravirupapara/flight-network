package com.ravi.searchFlights.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Flight Util class
 * 
 * @author Ravi
 *
 */
public class FlightUtil {

	public static final String SLASH_DATE_FORMATE = "MM/dd/yyyy' 'HH:mm:ss";
	public static final String DASH_DATE_FORMATE = "MM-dd-yyyy' 'HH:mm:ss";
	public static Log log = LogFactory.getLog(FlightUtil.class);
	public static List<SimpleDateFormat> knownPatterns = null;

	public static List<SimpleDateFormat> getDateFromates() {
		knownPatterns = new ArrayList<SimpleDateFormat>();
		knownPatterns.add(new SimpleDateFormat(DASH_DATE_FORMATE));
		knownPatterns.add(new SimpleDateFormat(SLASH_DATE_FORMATE));
		return knownPatterns;
	}

	/**
	 * Get common date string from different pattern date string
	 * 
	 * @param dateString
	 * @return
	 */
	public static String getCommonDate(String dateString) {
		SimpleDateFormat newFormat = new SimpleDateFormat(FlightUtil.SLASH_DATE_FORMATE);
		try {
			return newFormat.format(getDate(dateString));
		} catch (Exception e) {
			log.error(e);
		}
		return dateString;
	}

	/**
	 * Parse the date from date string
	 * 
	 * @param dateString
	 * @return
	 */
	public static Date getDate(String dateString) {
		for (SimpleDateFormat pattern : FlightUtil.getDateFromates()) {
			try {
				return new Date(pattern.parse(dateString).getTime());
			} catch (ParseException pe) {
				// Loop on
			}
		}
		return new Date();
	}
}
