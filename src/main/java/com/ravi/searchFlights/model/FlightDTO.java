package com.ravi.searchFlights.model;

import com.ravi.searchFlights.util.FlightUtil;

/**
 * Flight DTO
 * 
 * @author Ravi
 *
 */
public class FlightDTO implements Comparable<FlightDTO> {
	private String origin;
	private String destination;
	private String departure_time;
	private String destination_time;
	private String price;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDeparture_time() {
		return departure_time;
	}

	public void setDeparture_time(String departure_time) {
		this.departure_time = departure_time;
	}

	public String getDestination_time() {
		return destination_time;
	}

	public void setDestination_time(String destination_time) {
		this.destination_time = destination_time;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		return (this.origin.hashCode() + this.departure_time.hashCode() + this.destination.hashCode()
				+ this.destination_time.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FlightDTO) {
			return (((FlightDTO) obj).origin == origin && ((FlightDTO) obj).departure_time == departure_time
					&& ((FlightDTO) obj).destination == destination
					&& ((FlightDTO) obj).destination_time == destination_time);
		}
		return false;
	}

	@Override
	public int compareTo(FlightDTO o) {
		int i = price.compareTo(o.price);
		if (i != 0)
			return i;
		return FlightUtil.getDate(departure_time).compareTo(FlightUtil.getDate(o.departure_time));
	}
}
