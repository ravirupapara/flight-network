<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<%@ page isELIgnored="false" %>
</head>
<body>
	<h2>Search Flight</h2>
	<c:if test="${not empty flightList}">
		<c:forEach items="${flightList}" var="flight">
			<div><b>${flight.getOrigin()} --> ${flight.getDestination()} (${flight.getDeparture_time()} --> ${flight.getDestination_time()}) - ${flight.getPrice()}</b></div>
		</c:forEach>
	</c:if>
	<c:if test="${empty flightList}">
		<div><b>No Flights Found for ${origin} --> ${destination}</b></div>
	</c:if>
</body>
</html>
