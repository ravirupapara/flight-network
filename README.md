# SearchFlight

Search the flight based on origin and destination from the different flight list provider.

### Content

  - FlightController
  - FlightDTO
  - FlightUtil


FlightController:

  - Get the origin and destination parameter from request
  - Read the different flight provider list and convert to FlightDTO objects(CSV to Bean) using opencsv Library.
  - Add all the FlightDTO object in single general list.
  - Find the matching FlightDTO object from the list and set departure and destination date format identical to all.
  - Add all the matching object in map using key as hash value of object to remove dupicate entry.
  - Sort the final list of FlightDTO objects based on lowest price and earliest flight respectively.
  - Add the sorted list in the modal to display the flight objects.

FlightDTO:

  - Add all the property of the Flight.
  - Override the hashcode and equals method to find the duplicate object using hash value.
  - implements the Comparable interface to sort the Flight object.

FlightUtil:

  - Flight utility class for date conversion
